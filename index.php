<?php
/**
 * [travel with me project]
 *
 * @author Ahmed Mostafa <ahmed.mostafaa.k@gmail.com>
 */
require_once __DIR__ . '/vendor/autoload.php';

use Travel\VehicleFactory;

$vechicles= ['car', 'bus', 'motor cycle'];
$factory = new VehicleFactory;
$distance = 120;
foreach ($vechicles as $value) {
    $vechicle = $factory->createVechicle($value);
    echo "Vehicle ".ucfirst($vechicle->getType()).', Time spent: '.
    $vechicle->getTimeSpent($distance).' hr(s), Fuel stop: '.
    $vechicle->getFuelStopNeed($distance).'<br/>';
}
