<?php
/**
 * [parse test]
 * 
 * @author Ahmed Mostafa <ahmed.mostafaa.k@gmail.com>
 */
use Travel\VehicleFactory;
use Travel\Vehicle;
use Travel\Car;
use Travel\Bus;
use Travel\MotorCycle;
use PHPUnit\Framework\TestCase;

/**
 * Class for test Parser
 *
 * @author Ahmed Mostafa <ahmed.mostafaa.k@gmail.com>
 */
class VechicleTest extends TestCase
{
    protected $testcases = [
        [
            'name' => 'car',
            'time_spent' => 1.2,
            'fuel_stop' => 0.38,
        ],
        [
            'name' => 'bus',
            'time_spent' => 1.5,
            'fuel_stop' => 0.12,
        ],
        [
            'name' => 'motor cycle',
            'time_spent' => 2,
            'fuel_stop' => 2.4,
        ],
        
    ];

    /**
     * [$distance description]
     * 
     * @var integer $distance
     */
    private $distance = 120;

    /**
     * [testTimeSpentFormEveryVechilce]
     *
     * @return void
     */
    public function testTimeSpentFormEveryVechilce():void
    {
        $vechicleFactory = new VehicleFactory;
        foreach ($this->testcases as $key => $value) {
            $vechile = $vechicleFactory->createVechicle($value['name']);
            $this->assertEquals(
                $value['time_spent'],
                $vechile->getTimeSpent($this->distance)
            );
        }
    }

    /**
     * [testFuelStoptFormEveryVechilce]
     *
     * @return void
     */
    public function testFuelStoptFormEveryVechilce():void
    {
        $vechicleFactory = new VehicleFactory;
        foreach ($this->testcases as $key => $value) {
            $vechile = $vechicleFactory->createVechicle($value['name']);
            $this->assertEquals(
                $value['fuel_stop'],
                $vechile->getFuelStopNeed($this->distance)
            );
        }
    }

    /**
     * [testTypeOfEveryVechilce]
     *
     * @return void
     */
    public function testTypeOfEveryVechilce():void
    {
        $vechicleFactory = new VehicleFactory;
        foreach ($this->testcases as $key => $value) {
            $vechile = $vechicleFactory->createVechicle($value['name']);
            $this->assertEquals(
                $value['name'],
                $vechile->getType()
            );
        }
    }
}
