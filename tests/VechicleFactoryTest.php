<?php
/**
 * [parse test]
 * 
 * @author Ahmed Mostafa <ahmed.mostafaa.k@gmail.com>
 */
use Travel\VehicleFactory;
use Travel\Car;
use Travel\Bus;
use Travel\MotorCycle;
use PHPUnit\Framework\TestCase;

/**
 * Class for test Parser
 *
 * @author Ahmed Mostafa <ahmed.mostafaa.k@gmail.com>
 */
class VechicleFactoryTest extends TestCase
{
    protected $testcases = [
        [
            'name' => 'car',
            'type' => Car::class,
        ],
        [
            'name' => 'bus',
            'type' => Bus::class,
        ],
        [
            'name' => 'motor cycle',
            'type' => MotorCycle::class,
        ],
        
    ];
    /**
     * [testReturnTypeOfVechiceFactory]
     *
     * @return void
     */
    public function testReturnTypeOfVechiceFactory():void
    {
        $vechicleFactory = new VehicleFactory;
        foreach ($this->testcases as $key => $value) {
            $vechile = $vechicleFactory->createVechicle($value['name']);
            $this->assertInstanceOf($value['type'], $vechile);
        }
        $this->expectException(\Exception::class);
        $vechile = $vechicleFactory->createVechicle('plane');
        
    }
}
