<?php
/**
 * Bus Vechicle class
 *
 * @author Ahmed Mostafa <ahmed.mostafaa.k@gmail.com>
 */

namespace Travel;

use Travel\Vechicle;

/**
 * Bus Vechicle class
 *
 * @author Ahmed Mostafa <ahmed.mostafaa.k@gmail.com>
 */
class Bus extends Vechicle
{
    /**
     * Vechicle speed
     *
     * @var int $speed
     */
    protected $speed = 80;

    /**
     * [fuel tank cap]
     *
     * @var int $fuelTankCapicity
     */
    protected $fuelTankCapacity = 200;

    /**
     * [consuption rate]
     *
     * @var int $fuelConsumptionRate
     */
    protected $fuelConsumptionRate = 5;

    /**
     * [vechicle type]
     *
     * @var string $type
     */
    protected $type = 'bus';
}
