<?php
/**
 * Bus Vechicle class
 *
 * @author Ahmed Mostafa <ahmed.mostafaa.k@gmail.com>
 */
namespace Travel;

use Travel\Contract\VechicleInterface;
use Travel\Contract\VechicleFactoryInterface;
use Travel\Car;
use Travel\Bus;
use Travel\MotorCycle;

/**
 *  Vechicle Factory class
 *
 * @author Ahmed Mostafa <ahmed.mostafaa.k@gmail.com>
 */
class VehicleFactory implements VechicleFactoryInterface
{
    /**
     * [createVechicle description]
     *
     * @param string $vechicle [description]
     *
     * @return VechicleInterface [description]
     */
    public function createVechicle(string $vechicle):VechicleInterface
    {
        $vechicle = strtolower($vechicle);
        switch ($vechicle) {
        case 'car':
            return new Car;
        case 'bus':
            return new Bus;
        case 'motor cycle':
            return new MotorCycle;
        default:
            throw new \Exception("Undefined Vechicle Type", 1);
        }
    }
}
