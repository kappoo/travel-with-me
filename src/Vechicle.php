<?php
/**
 * Vechicle Base class
 *
 * @author Ahmed Mostafa <ahmed.mostafaa.k@gmail.com>
 */
namespace Travel;

use Travel\Contract\VechicleInterface;

/**
 * Vechicle Base class
 *
 * @author Ahmed Mostafa <ahmed.mostafaa.k@gmail.com>
 */
abstract class Vechicle implements VechicleInterface
{
    /**
     * [vechicle type]
     *
     * @var string $type
     */
    protected $type;

    /**
     * Vechicle speed
     *
     * @var int $speed
     */
    protected $speed;

    /**
     * [fuel tank cap]
     *
     * @var int $fuelTankCapicity
     */
    protected $fuelTankCapacity;

    /**
     * [consuption rate]
     *
     * @var int $consumptionRate
     */
    protected $fuelConsumptionRate;

    /**
     * [getTimeSpent description]
     *
     * @param int $distance [description]
     *
     * @return int              [description]
     */
    public function getTimeSpent(int $distance):float
    {
        return round($distance / $this->speed, 2);
    }

    /**
     * [getFuelStopNeed description]
     *
     * @param int $distance [description]
     *
     * @return double            [description]
     */
    public function getFuelStopNeed(int $distance):float
    {
        return
        round($distance / ($this->fuelConsumptionRate * $this->fuelTankCapacity), 2);
    }

    /**
     * [getType description]
     *
     * @return string
     */
    public function getType():string
    {
        return $this->type;
    }
}
