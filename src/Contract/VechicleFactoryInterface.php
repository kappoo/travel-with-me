<?php
/**
 * Bus Vechicle class
 *
 * @author Ahmed Mostafa <ahmed.mostafaa.k@gmail.com>
 */
namespace Travel\Contract;

use Travel\Contract\VechicleInterface;

/**
 *  Vechicle Factory interface
 *
 * @author Ahmed Mostafa <ahmed.mostafaa.k@gmail.com>
 */
interface VechicleFactoryInterface
{
    /**
     * [createVechicle description]
     *
     * @param string $vechicle [description]
     *
     * @return VechicleInterface [description]
     */
    public function createVechicle(string $vechicle):VechicleInterface;
}
