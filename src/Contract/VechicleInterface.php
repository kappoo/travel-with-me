<?php
/**
 * 
 * @author Ahmed Mostafa <ahmed.mostafaa.k@gmail.com>
 */
namespace Travel\Contract;

/**
 * Vechicle contract 
 * 
 * @author Ahmed Mostafa <ahmed.mostafaa.k@gmail.com>
 */
interface VechicleInterface
{
    /**
     * [getTimeSpent description]
     * 
     * @param int $distance [description]
     * 
     * @return float              [description]
     */
    public function getTimeSpent(int $distance):float;

    /**
     * [getFuelStopNeed description]
     * 
     * @param int $distance [description]
     *
     * @return float            [description]
     */
    public function getFuelStopNeed(int $distance):float;

    /**
     * [getType description]
     *
     * @return string
     */
    public function getType():string;
}
