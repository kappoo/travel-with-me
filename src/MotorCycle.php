<?php
/**
 * Vechicle Base class
 *
 * @author Ahmed Mostafa <ahmed.mostafaa.k@gmail.com>
 */
namespace Travel;

use Travel\Vechicle;

/**
 * Motor Cycle Vechicle class
 *
 * @author Ahmed Mostafa <ahmed.mostafaa.k@gmail.com>
 */
class MotorCycle extends Vechicle
{
    /**
     * Vechicle speed
     *
     * @var int $speed
     */
    protected $speed = 60;

    /**
     * [fuel tank cap]
     *
     * @var int $fuelTankCapicity
     */
    protected $fuelTankCapacity = 5;

    /**
     * [consuption rate]
     *
     * @var int $fuelConsumptionRate
     */
    protected $fuelConsumptionRate = 10;

    /**
     * [vechicle type]
     *
     * @var string $type
     */
    protected $type = 'motor cycle';
}
