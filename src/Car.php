<?php
/**
 * Car class
 *
 * @author Ahmed Mostafa <ahmed.mostafaa.k@gmail.com>
 */
namespace Travel;


use Travel\Vechicle;

/**
 * Car Vechicle class
 *
 * @author Ahmed Mostafa <ahmed.mostafaa.k@gmail.com>
 */
class Car extends Vechicle
{
    /**
     * Vechicle speed
     *
     * @var int $speed
     */
    protected $speed = 100;

    /**
     * [fuel tank cap]
     *
     * @var int $fuelTankCapicity
     */
    protected $fuelTankCapacity = 40;

    /**
     * [consuption rate]
     *
     * @var int $fuelConsumptionRate
     */
    protected $fuelConsumptionRate = 8;

    /**
     * [vechicle type]
     *
     * @var string $type
     */
    protected $type = 'car';
}
